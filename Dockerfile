FROM python:3
RUN mkdir /code
WORKDIR /code
COPY . /code
RUN pip install -r requirements.txt --no-cache
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]