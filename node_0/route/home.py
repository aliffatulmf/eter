from django.shortcuts import render
from django.contrib.auth.models import User
from django.db.models import Sum
from django.utils import timezone
from django.views import View
from datetime import timedelta
from node_0.models import Order, Collection, orderStatus

class Dash(View):

    def get_order(self, *args, **kwargs):
        return Order.objects.filter(username=User.objects.get(id=self.request.user.id))
    
    def get_earn(self, *args, **kwargs):
        datetime_start = timezone.localtime(timezone.now()) - timedelta(weeks=4)
        datetime_end = timezone.localtime(timezone.now())
        return self.get_order().filter(status=orderStatus.objects.get(code='D'), created_at__range=(datetime_start, datetime_end)).aggregate(total=Sum('profit'))
    
    def get_library(self, *args, **kwargs):
        return Collection.objects.filter(username=User.objects.get(id=self.request.user.id)) 

    def get(self, request, *args, **kwargs):
        data = {'record': self.get_order(), 'earn': self.get_earn(), 'lib': self.get_library()}

        return render(request, 'index2.html', {'data': data})