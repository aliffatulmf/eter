from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from node_0.models import Collection, Toko, Order, orderStatus
from django.views import View
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.conf import settings
from django.db import transaction, IntegrityError
from node_0.forms import UploadFileForms
import threading
import csv
import os


class AddCollections(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'collections/add_collections.html')

    def post(self, request, *args, **kwargs):
        form = UploadFileForms(request.POST, request.FILES)

        if form.is_valid():
            username = User.objects.get(id=request.user.id)
            title = form.cleaned_data['title']
            filename = form.cleaned_data['uploadFile']

            entry = Collection(username=username,
                               title=title,
                               filename=filename)
            try:
                with transaction.atomic():
                    entry.save()
            except IntegrityError:
                return IntegrityError('Error')

            return HttpResponse(status=201)
        return HttpResponseBadRequest()


class ShowCollections(View):
    def wait_for_deleting_file(self, obj):
        if obj is None:
            raise ProcessLookupError('Object empty.')
        os.remove(settings.MEDIA_ROOT + str(obj.filename))
        return

    def get(self, request, *args, **kwargs):
        data = Collection.objects.filter(username=User.objects.get(
            id=request.user.id)).order_by('-created_at')
        return render(request, 'collections/show_collections.html',
                      {'data': data})

    def delete(self, request, *args, **kwargs):
        try:
            col_obj = Collection.objects.get(
                pk__exact=request.GET.get('id'),
                username=User.objects.get(pk=request.user.id))

            t1 = threading.Thread(name='deletion-file',
                                  target=self.wait_for_deleting_file,
                                  args=(col_obj, ))
            t1.start()
            col_obj.delete()
        except Collection.DoesNotExist:
            return HttpResponseBadRequest()

        return HttpResponse(status=204)


class ReadCollections(View):
    def get(self, request, *args, **kwargs):
        entry = Collection.objects.get(id=request.GET.get('id'))

        try:
            with open(settings.MEDIA_ROOT + str(entry.filename),
                      encoding='UTF-8') as _:
                read = csv.DictReader(_, delimiter=',')
        except Exception:
            raise Exception('Read file error')

        return render(request, 'collections/col.html', {'data': read})


class DownloadCollection(View):
    def post(self, request, *args, **kwargs):
        entry = Collection.objects.get(
            id=request.GET.get('id'),
            username=User.objects.get(id=request.user.id))

        if entry.exists():
            url = str(settings.MEDIA_URL, entry.filename)
            return JsonResponse({'status': 200, 'url': url})

        return HttpResponseBadRequest()