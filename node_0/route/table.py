from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseBadRequest, Http404
from django.db import transaction
from django.views.decorators.cache import never_cache
from node_0.filters import TableFilter
from node_0.models import Order, orderStatus, Toko
import json


class Tables(View):
    template_name = 'table.html'

    def get(self, request, *args, **kwargs):
        try:
            ORDER = Order.objects.filter(username=User.objects.get(
                pk=request.user.id))
            SHOP = Toko.objects.filter(username=User.objects.get(
                pk=request.user.id))
            STATUS = orderStatus.objects.all()

            FILTER = TableFilter(request.GET,
                                 queryset=ORDER.order_by('created_at'))
            pagint = Paginator(FILTER.qs,
                               per_page=15).get_page(request.GET.get('page'))

            data = {
                'data': pagint,
                'shops': SHOP.order_by('name'),
                'status': STATUS
            }
        except:
            return Http404('Parameter error.')
        return render(request, self.template_name, data)

    def delete(self, request, *args, **kwargs):
        try:
            target = json.loads(request.body)['target'].split('.')
            ORDER = Order.objects.filter(
                username=User.objects.get(pk=request.user.id),
                orderid__exact=target[0],
                shop=Toko.objects.get(username=User.objects.get(
                    pk=request.user.id, username=request.user.username),
                                      name__exact=target[1]),
                name__exact=target[2])
            ORDER.delete()
        except Exception:
            return HttpResponseBadRequest()

        return HttpResponse(status=204)