from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.views import View
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.db import transaction, Error, IntegrityError
from node_0.models import Collection, Order, orderStatus, Toko
from node_0.forms import OrderForms, ShopForms, UpdateOrderForms
import os
import json


class OrderForm(View):
    def get(self, request, *args, **kwargs):
        SHOP = Toko.objects.filter(username=User.objects.get(
            id=request.user.id))
        STATUS = orderStatus.objects.all()

        data = {'data': {'shop': SHOP, 'status': STATUS}}
        return render(request, 'form_order.html', data)

    def post(self, request, *args, **kwargs):
        form = OrderForms(request.POST)

        if form.is_valid():
            entry = Order(
                username=User.objects.get(pk=request.user.id),
                name=form.cleaned_data['name'],
                orderid=form.cleaned_data['orderid'],
                shop=Toko.objects.get(pk=form.cleaned_data['shop']),
                buy=form.cleaned_data['buy'],
                sell=form.cleaned_data['sell'],
                profit=form.cleaned_data['sell'] - form.cleaned_data['buy'],
                status=orderStatus.objects.get(pk=form.cleaned_data['status']))

            try:
                with transaction.atomic():
                    entry.save()
            except IntegrityError:
                return HttpResponseBadRequest()
        return HttpResponse(status=201)


class ShopForm(View):
    def shop_query(self, **kwargs):
        return Toko.objects.filter(username=User.objects.get(
            id=self.request.user.id))

    def get(self, request, *args, **kwargs):
        return render(request, 'form_toko.html',
                      {'shops': self.shop_query().order_by('name')})

    def post(self, request, *args, **kwargs):
        form = ShopForms(request.POST)

        if form.is_valid():
            entry = Toko(username=User.objects.get(id=request.user.id),
                         name=form.cleaned_data['name'])

            try:
                with transaction.atomic():
                    entry.save()

            except BaseException:
                return HttpResponseBadRequest()
            return JsonResponse(
                {
                    'data': {
                        'id': self.shop_query().last().id,
                        'username': request.user.username,
                        'name': self.shop_query().last().name
                    },
                    'status': 201,
                    'statusText': 'Created'
                },
                status=201)

        return HttpResponseBadRequest()

    def delete(self, request, *args, **kwargs):
        request_json = json.loads(request.body)['target'].split('.')[1]
        entry = Toko.objects.filter(
            username=User.objects.get(id=request.user.id), id=request_json)

        if entry.exists():
            try:
                with transaction.atomic():
                    entry[0].delete()
            except IntegrityError:
                return IntegrityError('Record not found.')
        return HttpResponse(status=204)


class UpdateForm(View):
    def form_session_check(self, key):
        if type(key) is not str:
            raise EOFError('key must be String.')
        else:
            return key in self.request.session

    def get(self, request, *args, **kwargs):
        if request.GET.get('record'):
            request.session['orderid_record'] = request.GET.get('record')
            request.session.set_expiry(0)
        else:
            return redirect('table')

        SHOP = Toko.objects.filter(username=User.objects.get(
            id=request.user.id))
        ORDER_GET = Order.objects.get(
            orderid=request.session['orderid_record'],
            username=User.objects.get(id=request.user.id))
        STATUS = orderStatus.objects.all()

        data = {
            'shops': SHOP.order_by('name'),
            'status': STATUS.order_by('name'),
            'orders': ORDER_GET,
        }

        return render(request, 'form_update.html', {'data': data})

    def post(self, request, *args, **kwargs):
        form = UpdateOrderForms(request.POST)

        ORDER_POST = Order.objects.get(
            orderid=request.session['orderid_record'],
            username=User.objects.get(id=request.user.id))
        if form.is_valid():
            # default value
            shop = Toko.objects.get(
                id=ORDER_POST.shop.id,
                username=User.objects.get(pk=request.user.id))
            status = orderStatus.objects.get(id=ORDER_POST.status.id)
            buy = form.cleaned_data['buy']
            sell = form.cleaned_data['sell']
            profit = sell - buy
            awb = None

            # if shop and status is None
            if form.cleaned_data['shop'] is not None:
                shop = Toko.objects.get(id=form.cleaned_data['shop'])
            if form.cleaned_data['status'] is not None:
                status = orderStatus.objects.get(
                    id=form.cleaned_data['status'])
            if form.cleaned_data['awb'] is not None:
                awb = form.cleaned_data['awb']

            entry = Order.objects.filter(
                orderid=request.session['orderid_record'],
                username=User.objects.get(pk=request.user.id))

            if entry.count() != 1:
                raise Error('Entry more than one')
            try:
                with transaction.atomic():
                    entry.update(shop=shop,
                                 status=status,
                                 buy=buy,
                                 sell=sell,
                                 profit=profit,
                                 awb=awb)

            except IntegrityError:
                return HttpResponseBadRequest()

            if self.form_session_check('orderid_record'):
                del request.session['orderid_record']

            return redirect('table')
