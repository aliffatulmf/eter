from django.shortcuts import render, redirect
from django.http import *
from django.contrib.auth.models import User
from django.db import transaction, Error, IntegrityError
from django.views import View
from django.template import RequestContext
from node_0.models import Toko, orderStatus, Order
from node_0.forms import UpdateOrderForm

class FormUpdate(View):

    def check_record_param(self, request):
        if request.GET.get('record'):
            request.session['orderid_record'] = request.GET.get('record')
            request.session.set_expiry(0)
        else:
            redirect('table')

    def form_session_check(self, request, key):
        if type(key) != str:
            raise EOFError('key must be string')
        else:
            return key in request.session

    def get(self, request, *args, **kwargs):
        self.check_record_param(request)
        print(request)

        SHOP = Toko.objects.filter(username=User.objects.get(id=request.user.id))
        ORDER_GET = Order.objects.get(orderid=request.session['orderid_record'], username=User.objects.get(id=request.user.id))
        STATUS = orderStatus.objects.all()

        data = {
            'shops': SHOP.order_by('name'),
            'status': STATUS.order_by('id'),
            'orders': ORDER_GET,
        }

        return render(request, 'form_update.html', {'data': data})
    
    def post(self, request, *args, **kwargs):
        form = UpdateOrderForm(request.POST)

        ORDER_POST = Order.objects.get(orderid=request.session['orderid_record'], username=User.objects.get(id=request.user.id))
        if form.is_valid():
            # default value
            shop = Toko.objects.get(id=ORDER_POST.shop.id, username=User.objects.get(pk=request.user.id))
            status = orderStatus.objects.get(id=ORDER_POST.status.id)
            buy = form.cleaned_data['buy']
            sell = form.cleaned_data['sell']
            profit = sell - buy
            awb = form.cleaned_data['awb']

            # if shop and status is None
            if form.cleaned_data['shop'] is not None:
                shop = Toko.objects.get(id=form.cleaned_data['shop'])
            if form.cleaned_data['status'] is not None:
                status = orderStatus.objects.get(id=form.cleaned_data['status'])
            
            entry = Order.objects.filter(orderid=request.session['orderid_record'], username=User.objects.get(pk=request.user.id))
            if entry.count() != 1:
                raise Error('Entry more than one')
            try:
                with transaction.atomic():
                    entry.update(
                        shop=shop,
                        status=status,
                        buy=buy,
                        sell=sell,
                        profit=profit,
                        awb=awb
                    )
                    
            except IntegrityError:
                return HttpResponseBadRequest()
            if form_session_check('orderid_record'):
                del request.session['orderid_record']
            return redirect('table')
        raise Http404('Not found.')