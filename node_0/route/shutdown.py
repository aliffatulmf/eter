from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.http import HttpResponseBadRequest, HttpResponse
from threading import Thread
import os, time, signal


class Power:
    """
    Shutdown application.
    """
    def __init__(self):
        pass

    @csrf_exempt
    def __call__(self, request):
        if request.method == 'POST':
            os.system('clear') if os.name == 'posix' else os.system('cls')

            session = Thread(target=logout, args=(request, ))
            shutdown = Thread(target=self.killsession)

            session.start()
            print('\u001b[32mClearing data\u001b[0m')
            if request.session:
                request.session.flush()

            time.sleep(3)
            shutdown.start()

            return HttpResponse(status=200)

        if request.method == 'GET':
            return HttpResponseBadRequest()

    def killsession(self):
        print('\u001b[32mShutdown done.\u001b[0m')

        os.kill(os.getpid(), signal.SIGINT)
        return
