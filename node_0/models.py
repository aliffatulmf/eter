from django.db import models
from django.contrib.auth.models import User


class orderStatus(models.Model):
    code = models.CharField(max_length=2, unique=True)
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()


class Toko(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()


class Order(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    shop = models.ForeignKey(Toko, on_delete=models.CASCADE)
    status = models.ForeignKey(orderStatus, on_delete=models.PROTECT)
    name = models.CharField(max_length=45)
    orderid = models.CharField(max_length=45, unique=True)
    awb = models.CharField(max_length=45, null=True)
    sell = models.IntegerField()
    buy = models.IntegerField()
    profit = models.IntegerField()
    created_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()


class Collection(models.Model):
    # username = models.CharField(max_length=45)
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=45, unique=True)
    filename = models.FileField(upload_to='collections')
    created_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()