from django import forms
from django.core.validators import FileExtensionValidator
from node_0 import models


class OrderForms(forms.Form):
    name = forms.CharField(label='name', max_length=45, required=True)
    orderid = forms.CharField(label='orderid', max_length=45, required=True)
    shop = forms.IntegerField(label='shop', required=True)
    status = forms.IntegerField(label='status', required=True)
    buy = forms.IntegerField(label='buy', required=True)
    sell = forms.IntegerField(label='sell', required=True)


class UpdateOrderForms(forms.Form):
    shop = forms.IntegerField(label='shop', required=False)
    status = forms.IntegerField(label='status', required=False)
    buy = forms.IntegerField(label='buy', required=True)
    sell = forms.IntegerField(label='sell', required=True)
    awb = forms.CharField(label='awb', required=False)


class ShopForms(forms.Form):
    name = forms.CharField(label='name', max_length=45, required=True)


class UploadFileForms(forms.Form):
    title = forms.CharField(label='title', max_length=45, required=True)
    uploadFile = forms.FileField(label='uploadFile',
                                 required=True,
                                 validators=[FileExtensionValidator(['csv'])])


class UserForms(forms.Form):
    first_name = forms.CharField(label='first_name', max_length=45)
    last_name = forms.CharField(label='last_name', max_length=45)
    email = forms.CharField(label='email', max_length=45)
