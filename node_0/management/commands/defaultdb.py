from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from node_0.models import orderStatus
import time


class Command(BaseCommand):
    help = 'Default db data'

    # def add_arguments(self, parser):
    #     parser.add_argument('defaultdb', nargs='+', type=int)

    def handle(self, *args, **options):
        data = [
            {
                'code': 'P',
                'name': 'PENDING'
            },
            {
                'code': 'OP',
                'name': 'ON PROCESS'
            },
            {
                'code': 'D',
                'name': 'DELIVERED'
            },
            {
                'code': 'C',
                'name': 'CANCEL'
            },
        ]
        for x in data:
            entry = orderStatus(code=x['code'], name=x['name'])
            with transaction.atomic():
                entry.save()
            time.sleep(3)

        self.stdout.write(self.style.SUCCESS('Done'))
