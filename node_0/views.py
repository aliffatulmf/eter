from django.shortcuts import render, redirect
from django.http import *
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Sum
from django.db import transaction
from django.core.serializers import serialize
from django.views.decorators.csrf import csrf_exempt
# from django.core.files.base import ContentFile
# from django.core.files.storage import FileSystemStorage
from django.conf import settings
from node_0.filters import TableFilter
from node_0.models import *
from node_0.forms import *
from datetime import datetime
from random import randint
import csv
import os
# import requests
import json
#
# HOME
#


class home:

    def home(request):
        totalPayment = Order.objects.filter(username=User.objects.get(id=request.user.id)).count()
        totalPayment = 0 if totalPayment == None else totalPayment

        totalProfit = Order.objects.filter(username=User.objects.get(id=request.user.id), status=1).aggregate(total=Sum('profit'))
        totalProfit = 0 if totalProfit['total'] == None else totalProfit['total']

        return render(request, 'index2.html', {'totalPayment': totalPayment, 'totalProfit': totalProfit})


    def table2(request):
        ORDER = Order.objects.filter(username=User.objects.get(id=request.user.id))
        SHOP = Toko.objects.filter(username=User.objects.get(id=request.user.id))
        STATUS = orderStatus.objects.all()

        pagint = Paginator(TableFilter(request.GET, queryset=ORDER.order_by('id')).qs, per_page=1).get_page(request.GET.get('page'))

        data = {
            'data': pagint,
            'shops': SHOP.order_by('name'),
            'status': STATUS
        }

        return render(request, 'table.html', data)




















    def account(req):
        if req.method == 'GET':
            sv = User.objects.filter(username=req.user.username)
            return render(req, 'accounts/account.html', {'data': sv})
        elif req.method == 'POST':
            first_name = req.POST['first_name']
            last_name = req.POST['last_name']
            email = req.POST['email']
            try:
                sv = User.objects.filter(username=req.user.username)
                sv.update(first_name=first_name,
                          last_name=last_name,
                          email=email)
                messages.success(req, 'success')
                return redirect('/profile')
            except BaseException:
                messages.error(req, 'error')
                return redirect('/profile')

    def change_password(req):
        if req.method == 'GET':
            return render(req, 'accounts/password.html')
        elif req.method == 'POST':
            newPass = req.POST['new_password']
            newPassConf = req.POST['new_password_confirm']

            if newPass == newPassConf:
                setPass = User.objects.get(username=req.user.username)
                setPass.set_password(newPass)
                setPass.save()
                return redirect('/')
            else:
                return redirect('/change-password')

            return HttpResponse('now Same')


#
# FORM
#


#
# DELETE
#



    # def things(request):
    #     url = "https://xigoxah.pythonanywhere.com/api/"
    #     payload = ""
    #     headers = {
    #         'content-type': "application/json",
    #         'authorization': "Token 679631421758d84b2750c0a618886bb1708bedf5"
    #     }
    #     response = requests.request("GET", url, data=payload, headers=headers)
    #     a = json.loads(response.text)

    #     paginator = Paginator(a, 50)  # Show 25 contacts per page
    #     page = request.GET.get('page')
    #     contacts = paginator.get_page(page)

    #     return render(request, 'things.html', {'filter': contacts})
